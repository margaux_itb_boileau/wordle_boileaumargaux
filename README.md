# Wordle

## Definition of the project:
Code in Kotlin that allows you to play the game Wordle through the terminal. This single party game consists of guessing a specific five-letter word in a maximum of six tries.

## How to install and run the project:
Use git clone to clone this repository on your computer and open the project with IntelliJ IDEA. You will be able to run the program like any other kotlin project in IntelliJ IDEA, by clicking the run button in the upper-right corner of the screen.

## How does it work:
The word to guess will be chosen randomly from a list of 30 words.

The main code of the program will be repeated 6 times, one for each try.

First, the user's response will be obtained, imposing that it meets the following characteristics:
- It is string type
- It is not greater or less than 5 characters

If the entered word does not meet the requirements, an error will be thrown and the process will be repeated until a valid word is entered.

To keep a record of the words introduced, each answer will be added to a list.

For each word in the record, each letter of it will be printed in a box with the corresponding color:
- Green if the letter matches in value and position with that of the solution
- Yellow if the letter is within the solution but does not meet the previous condition
- Gray for the rest of cases, that is, the letter is not in the word

The program will continue based on the user's last response:
- If the last answer inserted is equal to the solution, the user will be told that he has won and the loop will be broken indicating the end of the program.
- If the answer is not correct and the user has more attempts left, they will be prompted to try again and the loop will continue.
- In attempt 6, the last one, if the answer is incorrect, the user will be told that he has lost and the program will end.

### License:  
This code is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE, see the [LICENSE.md](LICENSE) file for details.