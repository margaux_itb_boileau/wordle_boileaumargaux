/*
* AUTHOR: Margaux Boileau
* DATE: 2023/01/15
* TITLE: Wordle
* VERSION: 2.0
*/

import java.util.*

const val GREY = "\u001B[47m"
const val BLACK = "\u001B[30m"
const val GREEN = "\u001B[42m"
const val YELLOW = "\u001B[43m"
const val RESET = "\u001B[0m"

/**
 * Function that determines wether a string contains letters of the alphabet or other characters, works with lowercase and uppercase letters
 *
 * @param word the string to check if it contains only letters or not
 * @return bool value, true if the number of characters that are letters in str is equal to the size of str
 */
fun onlyLetters (word: String):Boolean {
    var n = 0
    for (i in word.uppercase()) {
        if (i.code in 65..90 || i=='Ñ') n++
    }
    return n==word.length
}

/**
 * Function that determines how many times a given letter is present in a word
 *
 * @param str the string we are looking to find how many repetitions of the letter are in
 * @param letter is the char to check how many times is repeated
 * @return int value, the number of times the letter is in the word
 */
fun repetitions(str: String, letter: Char):Int {
    var n = 0
    for (j in 0..str.lastIndex) {
        if (letter == str[j]) {
            n++
        }
    }
    return(n)
}

/**
 * Function that determines how many times a letter coincides in two words
 *
 * @param firstWord one of the two strings we are comparing
 * @param secondWord one of the two strings we are comparing
 * @param letter is the char to check how many times it coincides in value and position in both words
 * @return int value, the number of times the letter is in the word
 */
fun timesGuessed(firstWord: String, secondWord: String, letter: Char):Int {
    var n = 0
    for (i in firstWord.indices) {
        if (firstWord[i] == secondWord[i] && firstWord[i] == letter && secondWord[i] == letter) {
            n++
        }
    }
    return(n)
}

fun main() {
    println("Bienvenido a Wordle!\n" +
            "El objetivo es simple, adivinar la palabra oculta. La palabra tiene 5 letras no repetidas y tienes 6 intentos para adivinarla.\n" +
            "\n" +
            "Cada intento debe ser una palabra válida. En cada ronda el juego pinta cada letra de un color indicando si esa letra se encuentra o no en la palabra y si se encuentra en la posición correcta.\n" +
            "\n" +
            "· VERDE significa que la letra está en la palabra y en la posición CORRECTA.\n" +
            "\n" +
            "· AMARILLO significa que la letra está presente en la palabra pero en la posición INCORRECTA.\n" +
            "\n" +
            "· GRIS significa que la letra NO está en la palabra.\n" +
            "\n"
    )

    val repertoire = mutableListOf<String>("ARTES", "MUNDO", "CABLE", "ROBLE", "ZURDO", "PESAR", "QUEJA", "LIBRO", "APEGO", "BARDO", "BATIR", "BORDA", "CAUTO", "CETRO", "CISNE", "DOGMA", "DUCHA", "DUELO", "FOBIA", "FURIA", "GANSO", "GRANO", "JUEGO", "JUEZA", "MURAL", "ARAÑA", "POLLO", "AMIGA", "RATAS", "MANTA")
    val tries = 6
    val scanner = Scanner(System.`in`)
    var input: String
    println(repertoire.size)
    do {
        println("¡Comencemos!\n" + "Inserta tu palabra:\n")
        val solution = repertoire.random()
        val record = arrayListOf<String>()
        for (j in 1..tries) {

            //get input & add it on the record
            do {
                input = scanner.next().uppercase()
                if (!onlyLetters(input)) println("La palabra no puede contener números o carácteres especiales, solo letras")
                else if (input.length != 5) println("La palabra no puede tener más o menos de 5 letras")
            } while (input.length != 5 || !onlyLetters(input))
            record.add(input)

            //for every word in the record, print each letter in a box with the appropriate color
            for (word in record) {

                for (k in input) print("┏━━━━━┓")
                println()

                var count = 0
                for (i in 0..4) {
                    if (word[i] == solution[i]) {
                        print("┃" + BLACK + GREEN + "  ${word[i]}  " + RESET + "┃")
                    }
                    else if (word[i] in solution) {
                        if (repetitions(word,word[i])<=repetitions(solution,word[i])) {
                            print("┃" + BLACK + YELLOW + "  ${word[i]}  " + RESET + "┃")
                        }
                        else if (count<repetitions(solution,word[i])-timesGuessed(solution,word,word[i])) {
                            print("┃" + BLACK + YELLOW + "  ${word[i]}  " + RESET + "┃")
                            count++
                        }
                        else print("┃" + BLACK + GREY + "  ${word[i]}  " + RESET + "┃")
                    }
                    else {
                        print("┃" + BLACK + GREY + "  ${word[i]}  " + RESET + "┃")
                    }
                }
                println()

                for (k in input) print("┗━━━━━┛")
                println()
            }

            //if the answer is incorrect & it's not your last try: try again
            if (input == solution) {
                println("¡Has ganado! :D")
                break
            }
            if (j != 6 ) println("Inténtalo de nuevo:")
            else println("Has perdido :(")
        }
        println("Fin del juego.\n¿Quieres volver a jugar?")
        input = scanner.next().uppercase()
    } while(input!="NO" || input!="N")

}