import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class WordleKtTest {

    @Test
    fun onlyLetters() {
        assertEquals(false, onlyLetters("12345"))
        assertEquals(true, onlyLetters("araña"))
        assertEquals(true, onlyLetters("AMIGA"))
        assertEquals(false, onlyLetters(":*`$"))
    }

    @Test
    fun repetitions() {
        assertEquals(2, repetitions(("amiga"),'a'))
        assertEquals(5, repetitions(("aaaaa"),'a'))
        assertEquals(0, repetitions(("libro"),'a'))
    }

    @Test
    fun timesGuessed() {
        assertEquals(1, timesGuessed("amiga", "guapa", 'a'))
        assertEquals(0, timesGuessed("amiga", "libro", 'a'))
        assertEquals(2, timesGuessed("araña", "aaraa", 'a'))
    }
}